from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from django.shortcuts import redirect

class HomeView(TemplateView):
    template_name = 'home/index.html'

class LoginInterfaceView(LoginView):
    template_name = 'home/login.html'
    
class SignupView(CreateView):
    form_class = UserCreationForm
    template_name = 'home/register.html'
    success_url = '/project'

    def get(self, request, *arg, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('project')
        return super().get(request, *arg, **kwargs)

def logout(request):
    return render(request, 'home/logout.html', {})
