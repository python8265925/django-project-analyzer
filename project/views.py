from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, ListView, UpdateView
from django.views.generic.edit import DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Project
from .forms import ProjectForm

class ProjectCreateView(CreateView):
    model = Project
    success_url = '/project'
    form_class = ProjectForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

class ProjectDeleteView(DeleteView):
    model = Project
    success_url = '/project'
    template_name = 'project/project_delete.html'

class ProjectDetailView(DetailView):
    model = Project
    context_object_name = 'project'

class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = 'project'
    template_name = 'project/project_list.html'
    login_url = '/admin'

    def get_queryset(self):
        return self.request.user.project.all()

class ProjectUpdateView(UpdateView):
    model = Project
    success_url = '/project'
    form_class = ProjectForm
