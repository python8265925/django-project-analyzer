from django import forms 
from .models import Project
from django.core.validators import ValidationError

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('title', 'notes', 'labor_cost', 'material_cost'
            , 'overhead_cost', 'project_benefit')
        widgets = {
            'title' : forms.TextInput(attrs={'class': 'form-control my-5'}),
            'notes': forms.Textarea(attrs={'class': 'form-control mb-5'}),
            'labor_cost' : forms.NumberInput(),
            'material_cost': forms.NumberInput(),
            'overhead_cost' : forms.NumberInput(),
            'project_benefit': forms.NumberInput(),
        }
        labels = {
            'notes' : "Project Description",
            'labor_cost' : "Labor Cost",
            'material_cost' : "Material Cost",
            'overhead_cost' : "Overhead Cost",
            'project_benefit': "Project Return",
        }

        def clean_title(self):
            title = self.cleaned_data['title']
            return title

        def clean_notes(self):
            notes = self.cleaned_data['notes']
            return notes

        def clean_labor_cost(self):        
            labor_cost = self.cleaned_data['labor_cost']
            return labor_cost

        def clean_material_cost(self):
            material_cost = self.cleaned_data['material_cost']
            return material_cost

        def clean_overhead_cost(self):
            overhead_cost = self.cleaned_data['overhead_cost']
            return overhead_cost

        def clean_project_benefit(self):
            project_benefit = self.cleaned_data['project_benefit']
            return project_benefit
        