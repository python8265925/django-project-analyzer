from django.db import models
from django.contrib.auth.models import User

class Project(models.Model):
    title = models.CharField(max_length=200)
    notes = models.TextField()
    labor_cost = models.FloatField()
    material_cost = models.FloatField()
    overhead_cost = models.FloatField()
    project_benefit = models.FloatField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="project")
    
    @property
    def project_profit(self):
        calculated_field = (self.project_benefit - self.total_cost)
        return calculated_field

    @property
    def project_profit_percent(self):
         calculated_field = (self.project_profit / self.total_cost * 100)
         return calculated_field

    @property
    def total_cost(self):
        calculated_field = (self.overhead_cost + self.material_cost + self.labor_cost)
        return calculated_field