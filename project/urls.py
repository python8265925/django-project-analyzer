from django.urls import path
from . import views

urlpatterns = [
    path('project/', views.ProjectListView.as_view(), name='project'),
    path('project/new', views.ProjectCreateView.as_view(), name='project.new'),
    path('project/<int:pk>', views.ProjectDetailView.as_view(), name='project.detail'),
    path('project/<int:pk>/delete', views.ProjectDeleteView.as_view(), name='project.delete'),
    path('project/<int:pk>/edit', views.ProjectUpdateView.as_view(), name='project.edit')
]