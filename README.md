# MIS525 - May 2024 - Django Adaptation
## Course Project: Information Systems Solution
### The Information system will perform project cost / benefit analysis. Managers will be empowered to make better decisions via alternatives comparison.
#### NOTE: For this project, I replaced the instructions to use parallel arrays with a Django Database.


## Home App
### Home Page (not logged in)
![Home (signed in) image](./readme_images/Home_no_login.png)
### Home Page (logged in)
![Home (not signed in) image](./readme_images/Home.png)
### Sign Up Page
![Sign Up image](./readme_images/signup.png)
### Login Page
![Login image](./readme_images/login.png)
### Login Page
![Logout image](./readme_images/logout.png)




## Project App
### Create Page
![Create image](./readme_images/Create.png)
### Project List (View) Page
![View_List image](./readme_images/View_List.png)
### Detail Page
![Detail image](./readme_images/Detail.png)
### Edit Page
![Edit image](./readme_images/Edit.png)
### Delete Page
![Edit image](./readme_images/Delete.png)
